package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private final int maxCapacity = 20;
    private List<FridgeItem> inventory = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return inventory.size();
    }

    @Override
    public int totalSize() {
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            return inventory.add(item);
        }
        else {
             return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (inventory.contains(item)) {
            inventory.remove(item);
        }
        else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        inventory.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (FridgeItem item : inventory) {
            if (item.hasExpired() == true) {
                expiredFood.add(item);
            }
        }
        for (FridgeItem item : expiredFood) {
            inventory.remove(item);
        }
        return expiredFood;
    }
}